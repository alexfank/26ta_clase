//const mysql = require('mysql2');
const { Sequelize, DataTypes, Model } = require('sequelize');

// Configuramos nuestra instancia de Sequelize

const sequelize = new Sequelize('acamicadb', 'root', '', {
    host: 'localhost',
    dialect: 'mysql',
});

// Verificamos que se conecte a la base de datos MySQL

sequelize   .authenticate()
.then(() => {
    console.log('Conexión satisfactoria');
})
.catch(()=>{
    console.log('Conexión con problemas');
});

class Usuarios extends Model {}

Usuarios.init(
   {
       nombre: DataTypes.STRING,
       apellidos: DataTypes.STRING,
   },
   {
       sequelize,
       modelName: 'usuarios',
       timestamps:false,//para remover las columnas de createdAt, updatedAt
   }
);

class Casas extends Model {}

   Casas.init(
       {
           nombre: DataTypes.STRING,
       },
       {
           sequelize,
           modelName: 'casas',
           timeStamps:false,
       }
   );

   // Asociación usando belongsTo
   // Si la llave foranea la va a tener la primera tabla usamos belongsTo
   Usuarios.belongsTo(Casas,{goreignKey:'id_casa'});
   // Si la llave foranea la va a tener la segunda tabla usamos hasOne
   // Usuarios.hasOne(Casas);

   (async () => {

    //Insertar datos
    await sequelize.sync({force:true});

    const datoPlaya = { nombre: 'Casas de la playa'};
    const dataInsertCasa = await Casas.create(datoPlaya);
    console.log(dataInsertCasa.toJSON());

    //Consultar, mostrará el último ingresado
    console.log(usuarioData.nombre);
    console.log(usuarioData.apellidos);

    // Actualizamos los valores

    usuarioData.nombre = "Manuela";
    usuarioData.apellidos = "Jiménez Update";

    //Guardamos los cambios para actualizar
    await usuarioData.save();

    //Volvemos a consultarl a dataset
    console.log(usuarioData.nombre);
    console.log(usuarioData.apellidos);

    //Eliminamos el registro
    //await usuariosData.destroy();

    //Buscar otro resultado

    const userOne = await Usuarios.findOne({
        where: {
            nombre: "Manuela",
        },
    });

    if (userOne === null) {
        console.log('Usuario no encontrado')
    } else {
        console.log('Usuario encontrado', userOne.toJSON());
    }

//Buscar varios

// Buscar muchos resultados
const Usuarios = await Usuarios.findAll({
    where: {
        nombre: 'Manuela',
    },
});

Usuarios.forEach((item) => {
    console.log(item.toJSON()); //Listo todos los resultados
});

let usuarioPlaya = await Usuarios.findAll({
    include:{
        model: Casas
    },
    attributes: ['nombre','apellidos']
});

usuarioPlaya.forEach((item) => {
    console.log('datos playa',item.toJSON()); // Listo todos los resultados)
});

})();